package main

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/jessevdk/go-flags"

	"brainfuck-interpreter/internal"
)

type Options struct {
	Output      string `short:"o" long:"output" description:"Output writer" default:"stdout"`
	Interpreter string `short:"i" long:"interpreter" default:"basic"`
	InputFile   string `short:"f" long:"input_file"`
	InputString string `short:"s" long:"input_string"`
}

var options Options

var parser = flags.NewParser(&options, flags.HelpFlag|flags.PassDoubleDash)

func main() {
	if _, err := parser.Parse(); err != nil {
		if !strings.HasPrefix(err.Error(), "Usage") {
			fmt.Fprintf(os.Stderr, "%v\n", err)
		}
		parser.WriteHelp(os.Stdout)
		os.Exit(1)
	}

	var d func()
	var err error
	var f io.Writer
	var i internal.Interpreter
	var r io.Reader

	if r, err, d = setupInput(&options); err != nil {
		printHelp(err)
	}
	if d != nil {
		defer d()
		d = nil
	}

	if i, err = setupInterpreter(&options, r); err != nil {
		printHelp(err)
	}

	if f, err, d = setupOutput(&options); err != nil {
		printHelp(err)
	}
	if d != nil {
		defer d()
	}

	i.Execute(f)
}

func setupInterpreter(opt *Options, r io.Reader) (internal.Interpreter, error) {
	switch opt.Interpreter {
	case "basic":
		return internal.NewBasicInterpreter(r)
	case "extended":
		return internal.NewExtendedInterpreter(r)
	}
	return nil, errors.New("unknown type of interpreter")
}

func setupInput(opt *Options) (io.Reader, error, func()) {
	if opt.InputFile == "" && opt.InputString == "" {
		return nil, errors.New("must be at lease one input source"), nil
	}

	if opt.InputFile != "" && opt.InputString != "" {
		return nil, errors.New("must be only one input source"), nil
	}

	if opt.InputString != "" {
		return strings.NewReader(opt.InputString), nil, nil
	}

	if opt.InputFile != "" {
		f, err := os.Open(opt.InputFile)
		if err != nil {
			return nil, err, nil
		}

		return f, nil, func() {
			if err := f.Close(); err != nil {
				panic(err)
			}
		}
	}

	return nil, errors.New("unknown input source"), nil
}

func setupOutput(opt *Options) (io.Writer, error, func()) {
	switch opt.Output {
	case "":
		return nil, errors.New("output must be defined"), nil
	case "stdout":
		return os.Stdout, nil, nil
	case "stderr":
		return os.Stderr, nil, nil
	}

	f, err := os.Open(opt.Output)
	if err != nil {
		return nil, err, nil
	}

	return f, nil, func() {
		if err := f.Close(); err != nil {
			panic(err)
		}
	}
}

func printHelp(err error) {
	fmt.Fprintln(os.Stderr, fmt.Sprintf("\033[36mERROR: \033[31m%v\033[0m", err))
	parser.WriteHelp(os.Stdout)
	os.Exit(1)
}
