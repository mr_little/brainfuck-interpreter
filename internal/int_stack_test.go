package internal

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIntStack(t *testing.T) {
	i := IntStack()
	assert.Equal(t, true, i.IsEmpty())

	i.Push(1)
	i.Push(2)
	assert.Equal(t, 2, len(i))

	i.Pop()
	assert.Equal(t, 1, len(i))
	i.Pop()

	assert.Equal(t, true, i.IsEmpty())
	i.Pop()
}
