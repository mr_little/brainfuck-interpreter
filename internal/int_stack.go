package internal

func IntStack() intStack {
	return make(intStack, 0)
}

type intStack []int

// IsEmpty: check if stack is empty
func (i *intStack) IsEmpty() bool {
	return len(*i) == 0
}

// Push a new value onto the stack
func (i *intStack) Push(o int) {
	*i = append(*i, o) // Simply append the new value to the end of the stack
}

// Remove and return top element of stack. Return false if stack is empty.
func (i *intStack) Pop() (int, bool) {
	if i.IsEmpty() {
		return -1, false
	} else {
		index := len(*i) - 1   // Get the index of the top most element.
		element := (*i)[index] // Index into the slice and obtain the element.
		*i = (*i)[:index]      // Remove it from the stack by slicing it off.
		return element, true
	}
}
