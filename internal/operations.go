package internal

import (
	"fmt"
	"io"
)

type Operation interface {
	Run(args *OperationArgs)
	Update(interface{})
}
type OperationArgs struct {
	w       io.Writer
	index   *int
	pointer *int
	cells   []int
}

func Inc() Operation {
	return &inc{}
}

type inc struct{}

func (i *inc) Run(args *OperationArgs) {
	args.cells[*args.pointer]++
}
func (i *inc) Update(n interface{}) {}

func Dec() Operation {
	return &dec{}
}

type dec struct{}

func (d *dec) Run(args *OperationArgs) {
	args.cells[*args.pointer]--
}
func (d *dec) Update(n interface{}) {}

func Jmp() Operation {
	return &jmp{}
}

type jmp struct {
	to int
}

func (j *jmp) Run(args *OperationArgs) {
	if args.cells[*args.pointer] <= 0 {
		*args.index = j.to
	}
}
func (j *jmp) Update(n interface{}) {
	j.to = n.(int)
}

func Ret(to int) Operation {
	return &ret{to: to}
}

type ret struct {
	to int
}

func (r *ret) Run(args *OperationArgs) {
	if args.cells[*args.pointer] > 0 {
		*args.index = r.to
	}
}
func (r *ret) Update(n interface{}) {
	r.to = n.(int)
}

func ShiftRight() Operation {
	return &sR{}
}

type sR struct{}

func (s *sR) Run(args *OperationArgs) {
	*args.pointer++
}
func (s *sR) Update(n interface{}) {}

func ShiftLeft() Operation {
	return &sL{}
}

type sL struct{}

func (s *sL) Run(args *OperationArgs) {
	*args.pointer--
}
func (s *sL) Update(n interface{}) {}

func Cout() Operation {
	return &cout{}
}

type cout struct{}

func (c *cout) Run(args *OperationArgs) {
	fmt.Fprint(args.w, string(args.cells[*args.pointer]))
}
func (c *cout) Update(n interface{}) {
}

func Cin() Operation {
	return &cin{}
}

type cin struct{}

func (c *cin) Run(args *OperationArgs) {
	var inString string
	fmt.Scanf("%s", &inString)

	if len(inString) > 0 {
		args.cells[*args.pointer] = int(inString[0])
	}
}
func (c *cin) Update(n interface{}) {}
