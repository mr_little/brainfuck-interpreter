package internal

import (
	"errors"
	"fmt"
	"io"
)

var pairedBracketMissed = errors.New("paired bracket missed")

type Interpreter interface {
	Execute(w io.Writer)
	String() string
}

type basicInterpreter struct {
	items []Operation
}

func (o *basicInterpreter) Add(n Operation) {
	o.items = append(o.items, n)
}
func (o *basicInterpreter) Execute(w io.Writer) {
	cells := make([]int, 1000)
	pointer := 0

	for loopIndex := 0; loopIndex < len(o.items); loopIndex++ {
		o.items[loopIndex].Run(&OperationArgs{w: w, cells: cells, index: &loopIndex, pointer: &pointer})
	}
}
func (o *basicInterpreter) String() string {
	res := ""

	for _, op := range o.items {
		res += fmt.Sprintf("{%p %v, %T}", op, op, op)
	}
	res += "\n"

	return res
}
func (o *basicInterpreter) UpdateItem(item int, n interface{}) {
	o.items[item].Update(n)
}
func NewBasicInterpreter(r io.Reader) (Interpreter, error) {
	buf := make([]byte, 1)

	var err error

	opList := &basicInterpreter{}
	iStack := IntStack()

	i := 0
	for {
		_, err = r.Read(buf)
		if err != nil {
			break
		}

		switch string(buf[0]) {
		case "+":
			opList.Add(Inc())
		case "-":
			opList.Add(Dec())
		case ">":
			opList.Add(ShiftRight())
		case "<":
			opList.Add(ShiftLeft())
		case ".":
			opList.Add(Cout())
		case ",":
			opList.Add(Cin())
		case "[":
			opList.Add(Jmp())
			iStack.Push(i)
		case "]":
			o, exists := iStack.Pop()
			if !exists {
				return nil, pairedBracketMissed
			}
			opList.Add(Ret(o))
			opList.UpdateItem(o, i)
		default:
			continue
		}
		i++
	}
	if len(iStack) > 0 {
		return nil, pairedBracketMissed
	}

	return opList, nil
}
