package internal

import (
	"bytes"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExtendedOperation(t *testing.T) {
	s := `++++++++*++++++++.`
	r := strings.NewReader(s)
	var got bytes.Buffer
	parsed, err := NewExtendedInterpreter(r)
	assert.NoError(t, err)
	parsed.Execute(&got)
	assert.Equal(t, "H", got.String())
}
