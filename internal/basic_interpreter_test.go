package internal

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHelloWorld(t *testing.T) {
	s := `++++++++++[>+>+++>+++++++>++++++++++<<<<-]>>>++.>+.+++++++..+++.<<++.>+++++++++++++++.>.+++.------.--------.<<+.<.`
	r := strings.NewReader(s)
	var got bytes.Buffer
	parsed, err := NewBasicInterpreter(r)
	assert.NoError(t, err)
	parsed.Execute(&got)
	assert.Equal(t, "Hello World!\n", got.String())
}

func TestMultiply(t *testing.T) {
	s := `++++++ number 1
>
+++++++ number 2
<
[ outer adder loop
  > copy second looping number
  [
    >+ real copy
    >+ backup to copy to original
    <<- decrement
  ]
  >> restore second loop after copy
  [
    <<+
    >>-
  ]
  < set pointer to helper secondary loop num
  [ do the incrementation (multiply)
    >>+
    <<-
  ]
  <<-
]
>>>>.`
	r := strings.NewReader(s)
	var got bytes.Buffer
	parsed, err := NewBasicInterpreter(r)
	assert.NoError(t, err)
	parsed.Execute(&got)
	assert.Equal(t, `*`, got.String())
}

func TestWithInput(t *testing.T) {
	content := []byte("?")
	tmpfile, err := ioutil.TempFile("", "")
	if err != nil {
		log.Fatal(err)
	}

	defer os.Remove(tmpfile.Name()) // clean up

	if _, err := tmpfile.Write(content); err != nil {
		log.Fatal(err)
	}

	if _, err := tmpfile.Seek(0, 0); err != nil {
		log.Fatal(err)
	}

	oldStdin := os.Stdin
	defer func() { os.Stdin = oldStdin }() // Restore original Stdin

	os.Stdin = tmpfile

	s := `+++[>+++++<-]>>+<[>>++++>++>+++++>+++++>+>>+<++[++<]>---]

>++++.>>>.+++++.>------.<--.+++++++++.>+.+.<<<<---.[>]<<.<<<.-------.>++++.
<+++++.+.>-----.>+.<++++.>>++.>-----.

<<<-----.+++++.-------.<--.<<<.>>>.<<+.>------.-..--.+++.-----<++.<--[>+<-]
>>>>>--.--.<++++.>>-.<<<.>>>--.>.

<<<<-----.>----.++++++++.----<+.+++++++++>>--.+.++<<<<.[>]<.>>

,[>>+++[<+++++++>-]<[<[-[-<]]>>[>]<-]<[<+++++>-[<+++>-[<-->-[<+++>-
[<++++[>[->>]<[>>]<<-]>[<+++>-[<--->-[<++++>-[<+++[>[-[-[-[->>]]]]<[>>]<<-]
>[<+>-[<->-[<++>-[<[-]>-]]]]]]]]]]]]]

<[
    -[-[>+<-]>]
    <[<<<<.>+++.+.+++.-------.>---.++.<.>-.++<<<<.[>]>>>>>>>>>]
    <[[<]>++.--[>]>>>>>>>>]
    <[<<++..-->>>>>>]
    <[<<..>>>>>]
    <[<<..-.+>>>>]
    <[<<++..---.+>>>]
    <[<<<.>>.>>>>>]
    <[<<<<-----.+++++>.----.+++.+>---.<<<-.[>]>]
    <[<<<<.-----.>++++.<++.+++>----.>---.<<<.-[>]]
    <[<<<<<----.>>.<<.+++++.>>>+.++>.>>]
    <.>
]>
,]

<<<<<.<+.>++++.<----.>>---.<<<-.>>>+.>.>.[<]>++.[>]<.
>[Translates brainfuck to C. Assumes no-change-on-EOF or EOF->0.
Generated C does no-change-on-EOF, and uses unistd.h read and write calls.
Daniel B Cristofani (cristofdathevanetdotcom)
http://www.hevanet.com/cristofd/brainfuck/]`

	r := strings.NewReader(s)
	var got bytes.Buffer

	parsed, err := NewBasicInterpreter(r)
	assert.NoError(t, err)
	parsed.Execute(&got)

	assert.Equal(t, `#include <unistd.h>
char r[65536],*e=r;
main(){
exit(0);
}
`, got.String())
}

func TestCyclicOperation(t *testing.T) {
	s := `++++++++++++++++++++++++++++++++++++++++++++++>
++++++++++++++++++++++++++++++++++++++++++>
+++++++++++++++++++++>>+++<<
[
  <<.>>>>
  [
    ->+>+<<[>[-]>[-]<<<]>>
    [<<<<<.>>>>><<+++>>[-]<[-]]
    <<
  ]
  <-
]`
	r := strings.NewReader(s)
	var got bytes.Buffer
	parsed, err := NewBasicInterpreter(r)
	assert.NoError(t, err)
	parsed.Execute(&got)
	assert.Equal(t, `...*...*...*...*...*...*...*`, got.String())
}

func TestSquare(t *testing.T) {
	s := `++++[>+++++<-]>[<+++++>-]+<+[
    >[>+>+<<-]++>>[<<+>>-]>>>[-]++>[-]+
    >>>+[[-]++++++>>>]<<<[[<++++++++<++>>-]+<.<[>----<-]<]
    <<[>>>>>[>>>[-]+++++++++<[>-<-]+++++++++>[-[<->-]+[<<<]]<[>+<-]>]<<-]<<-
]
[Outputs square numbers from 0 to 10000.
Daniel B Cristofani (cristofdathevanetdotcom)
http://www.hevanet.com/cristofd/brainfuck/]`
	r := strings.NewReader(s)
	var got bytes.Buffer
	parsed, err := NewBasicInterpreter(r)
	assert.NoError(t, err)
	parsed.Execute(&got)
	assert.Equal(t, `0
1
4
9
16
25
36
49
64
81
100
121
144
169
196
225
256
289
324
361
400
441
484
529
576
625
676
729
784
841
900
961
1024
1089
1156
1225
1296
1369
1444
1521
1600
1681
1764
1849
1936
2025
2116
2209
2304
2401
2500
2601
2704
2809
2916
3025
3136
3249
3364
3481
3600
3721
3844
3969
4096
4225
4356
4489
4624
4761
4900
5041
5184
5329
5476
5625
5776
5929
6084
6241
6400
6561
6724
6889
7056
7225
7396
7569
7744
7921
8100
8281
8464
8649
8836
9025
9216
9409
9604
9801
10000
`, got.String())
}
