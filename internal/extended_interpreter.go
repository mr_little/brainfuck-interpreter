package internal

import (
	"io"
)

func Square() Operation {
	return &square{}
}

type square struct{}

func (s *square) Run(args *OperationArgs) {
	args.cells[*args.pointer] *= args.cells[*args.pointer]
}
func (s *square) Update(n interface{}) {}

type extendedInterpreter struct {
	basicInterpreter
}

func NewExtendedInterpreter(r io.Reader) (Interpreter, error) {
	buf := make([]byte, 1)

	var err error

	opList := &extendedInterpreter{}
	iStack := IntStack()

	i := 0
	for {
		_, err = r.Read(buf)
		if err != nil {
			break
		}

		switch string(buf[0]) {
		case "+":
			opList.Add(Inc())
		case "-":
			opList.Add(Dec())
		case ">":
			opList.Add(ShiftRight())
		case "<":
			opList.Add(ShiftLeft())
		case ".":
			opList.Add(Cout())
		case ",":
			opList.Add(Cin())
		case "*":
			opList.Add(Square())
		case "[":
			opList.Add(Jmp())
			iStack.Push(i)
		case "]":
			o, exists := iStack.Pop()
			if !exists {
				return nil, pairedBracketMissed
			}
			opList.Add(Ret(o))
			opList.UpdateItem(o, i)
		default:
			continue
		}
		i++
	}
	if len(iStack) > 0 {
		return nil, pairedBracketMissed
	}

	return opList, nil
}
