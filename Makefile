

# Creates binary
build:
	go build -o ./cmd/bf ./cmd/main.go

# Runs tests
test-unit:
	go test ./... -v -race -count=1 -short

# test formats
test-format:
	[ "`go fmt  ./...`" = "" ]
